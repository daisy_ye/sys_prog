#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

int main()
{
    char c; 
    int in, out;

    in = open("file.in", O_RDONLY); 
    out = open("file.out", O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR);
    printf("in=%d,out=%d\n",in,out);
    while(read(in,&c,1) == 1)
        write(out,&c,1);
    close(in);
    close(out);
    exit(0);
}

